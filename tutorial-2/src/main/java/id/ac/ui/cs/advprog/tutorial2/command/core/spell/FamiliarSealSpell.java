package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    public FamiliarSealSpell(Familiar f) {
        super(f);
    }
    // TODO: Complete Me


    @java.lang.Override
    public void cast() {
        this.familiar.seal();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
