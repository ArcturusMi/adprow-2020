package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }
        //ToDo: Complete Me
        public void update(){
                Quest q = this.guild.getQuest();
                if(!q.getType().equals("R")){
                        this.getQuests().add(q);
                }
        }
}
