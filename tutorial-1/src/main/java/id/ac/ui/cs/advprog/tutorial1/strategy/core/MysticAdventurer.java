package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
        public MysticAdventurer(){
                AttackWithMagic magic = new AttackWithMagic();
                DefendWithShield shield = new DefendWithShield();
                this.setAttackBehavior(magic);
                this.setDefenseBehavior(shield);
        }

        public String getAlias(){
                return "I Am Spellslinger";
        }
}
