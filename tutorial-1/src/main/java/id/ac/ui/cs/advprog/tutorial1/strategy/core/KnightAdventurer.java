package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
        public KnightAdventurer(){
                AttackWithSword sword = new AttackWithSword();
                DefendWithArmor armor = new DefendWithArmor();
                this.setAttackBehavior(sword);
                this.setDefenseBehavior(armor);
        }

        public String getAlias(){
                return "I Am Steel";
        }
}
