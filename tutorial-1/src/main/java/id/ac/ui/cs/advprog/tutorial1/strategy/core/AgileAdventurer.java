package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete me
        public AgileAdventurer(){
                AttackWithGun gun = new AttackWithGun();
                DefendWithBarrier barrier = new DefendWithBarrier();
                this.setAttackBehavior(gun);
                this.setDefenseBehavior(barrier);
        }

        public String getAlias(){
                return "I Am Speed";
        }
}
