package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me
        public void update(){
                Quest q = this.guild.getQuest();
                if(!q.getType().equals("E")){
                        this.getQuests().add(q);
                }
        }
}
